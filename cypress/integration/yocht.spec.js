it('Do les things',() => {
    cy.visit('https://k-yachtingservices.com');
    cy.get(':nth-child(2) > :nth-child(2) > a > span').click();
    cy.contains('Apply').click();
    cy.get('.promo-info > .btn').click({force : true});
    cy.get('#email').type('adlene2@yopmail.com',{force : true});
    cy.get('#password').type('dandelion',{force : true});
    cy.get('#password_confirmation').type('dandelion',{force : true});
    cy.get('.checkbox > label').click({force : true});
    cy.get('#register-form > .btn').click({force : true});
    cy.get('#select2-gender-container').click();
    cy.get('.select2-dropdown > .select2-search > .select2-search__field').type('tr');
    cy.get('.select2-dropdown > .select2-search > .select2-search__field').type('{enter}');
    cy.get('#first_name').type('Adlene',{force : true});
    cy.get('#last_name').type('Drake',{force : true});
    cy.get('#current_location').type('Japan',{force : true});
    cy.get('.col-sm-8 > .input-field > .select2 > .selection > .select2-selection > .select2-selection__rendered > .select2-search > .select2-search__field').type('Sail',{force : true});
    cy.get('.col-sm-8 > .input-field > .select2 > .selection > .select2-selection > .select2-selection__rendered > .select2-search > .select2-search__field').type('{enter}',{force : true});
    cy.get('#select2-experience-container').click({force : true }),
    cy.get('.select2-dropdown > .select2-search > .select2-search__field').type('10',{force : true});
    cy.get('.select2-dropdown > .select2-search > .select2-search__field').type('{enter}',{force : true});
    cy.get('.col-md-offset-4 > .btn').click({force : true});

})
<?php namespace App\Tests\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class ClickTest extends WebTestCase {

    public function testClicks() {

        $client = static::createClient();

        $client->request('GET', '/clicks');

        $jsonRespone = json_decode($client->getResponse()->getContent());

        $this->assertEquals(200,$client->getResponse()->getStatusCode());

        $this->assertObjectHasAttribute("clicks", $jsonRespone);
        


    }
}